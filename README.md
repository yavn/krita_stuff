# Krita Stuff

This is a personal repo to host resources related to Krita development.

## Contents

- [Gap closing fill](gap_closing_fill/README.md) - Explanation of the fill algorithm
  and its Krita implementation.
