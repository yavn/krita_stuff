# Gap closing fill for Krita

1. [Intro](#intro)
2. [Attributions](#attributions)
3. [Problem description](#problem-description)
4. [Input data](#input-data)
   1. [Obtaining the opacity data](#obtaining-the-opacity-data)
   2. [Obtaining the distance data](#obtaining-the-distance-data)
5. [Executing a gap closing fill](#executing-a-gap-closing-fill)
6. [Optimization](#optimization)
   1. [The slow flood fill algorithm](#the-slow-flood-fill-algorithm)
   2. [The cost of filling small areas](#the-cost-of-filling-small-areas)
   3. [Memory allocation overhead](#memory-allocation-overhead)
7. [Abandoned ideas](#abandoned-ideas)

## Intro

This documents explains a gap closing fill implementation for Krita. The information is accurate
as of posting the initial MR for the feature, before code review feedback.

## Attributions

The core of the algorithm is the gap distance calculation code, which was taken largely
unchanged from the MyPaint project. That code can be found [here](https://github.com/mypaint/mypaint/blob/master/lib/fill/gap_detection.cpp). MyPaint code is copyrighted (C) 2018 by the MyPaint Development Team,
and distributed under GNU General Public License version 2.

The character drawings are based on references from [hidechannel2](https://www.youtube.com/@hidechannel2).

## Problem description

The gap closing fill refers to a "smart" bucket fill method that can detect gaps (discontinuities)
in the lineart and stop the fill before spilling through these gaps.

This problem typically affects comics, manga, animation, or illustration techniques that rely on filling
a draft or lineart with flat colors before moving on to shading.

![fill comparison](images/lineart_fill_comparison.png)

**Image 1:** Fill method comparison. *Left:* original lineart, *Middle:* regular bucket fill, *Right:* gap closing fill. \
The image on the right was achieved with a few clicks: one in the background, and four more to fill around the girl's
ponytails.

![lineart gap](images/lineart_gap_example.png)

**Image 2:** Gaps in the line drawing.

This tool lets the user select the pixel gap size that corresponds to the size of the lineart,
and then use a simple bucket tool to fill the intended regions quickly.

## Input data

In order to detect gaps and stop the fill, some data is needed before the fill operation begins.

1. An **opacity map** - this two-dimensional array tells us whether a pixel is transparent (can be filled),
   or is opaque (cannot be filled and stops the fill from spreading). \
   In this example, if we're filling the background, then the white pixels are transparent and the lineart is opaque.
   If the fill would begin in a black pixel that is a part of the lineart, then it would be considered transparent
   and the white surroundings would be opaque. Furthermore, opacity can depend on settings such as the threshold.

   ![opacity map](images/opacity_map.png)

   **Image 3:** A visualization of an opacity map. The pixels are either completely black (opaque) or white (transparent).

2. A **gap distance map** - this two-dimensional array contains the distance to the closest detected gap at each pixel.
   The closest distance is 1, and this value increases as we move away from a gap. There's also a special value,
   which we will call "infinite". It means that the pixel is too far away from any gaps and can be filled like a regular pixel.
   Note that for the prupose of the algorithm, jagged lines, indents, and corners (when lines meet at a sharp angle)
   are also considered "gaps". \
   Below is an example of a gap distance map for the same fragment of the image. This is generated at the gap size of 5 pixels.

   ![distance map](images/distance_map.png)

   **Image 4:** A visualization of a distance map. The lineart was colored gray for a better contrast.
   The red pixels correspond to the relative distance from the closest "gap". The most intensive red pixels are the lowest distance.

    The distance map can be a bit confusing to read for a realistic image, so here's another view of a more synthetic
    example. Again, the red areas correspond to the detected gaps, and can potentially block the fill from spreading.

    ![distance map](images/low_res_distance_map.png)

    **Image 5:** A visualization of a distance map. This is a very low resolution image enlarged 5 times.
    The gap size is 2 pixels.

With these two maps, we have everything we need to fill the image with gap closing. The method is explained further below.

### Obtaining the opacity data

Obtaining the opacity map is simple. Depending on how the image is represented, each pixel in the filled region must
be checked if it's opaque or not. Then that value is written back to the map.

***Implementation note***: In Krita the access to an image (a paint device) is provided thorugh iterators used with
conjunction with access, difference, and selection policies. In short, it involves some computation. Creating a simple
map of `uint8` can help greatly speed up the algorithm, especially with more erratic access patterns.

With opacity data ready, we can move on to distance map computation.

### Obtaining the distance data

For this part, I used MyPaint's algorithm as the base. The description below refers to the modified Krita version.

Let's first describe the basic algorithm, which is computed simply for every pixel of the image,
starting in the top-left corner (0, 0). The core pseudocode loop is presented further below.

Before the loop can start, every pixel must be initialized to the "infinite" value. In practice, it's just an arbitrary
big number, much bigger than any valid distance value we expect to compute.


```c++
for (int y = 0; y < imageHeight; ++y) {
    for (int x = 0; x < imageWidth; ++x) {
        if (isOpaque(x, y)) {
            gapDistanceSearch(x, y, gapSize, firstOctant_0_45);
            gapDistanceSearch(x, y, gapSize, secondOctant_45_90);
            gapDistanceSearch(x, y, gapSize, thirdOctant_90_135);
            gapDistanceSearch(x, y, gapSize, fourthOctant_135_180);
        }
    }
}
```

If the (x, y) pixel is opaque, we start searching for gaps and updating *surrounding pixels'* distance values in four separate
octants (45 degree circle sectors). These four octants together form a half-circle to the right of the pixel, with the
(x, y) pixel at the center of the half-circle.

![distance search at a point](images/distance_search_at_point.png)

**Image 6:** The four octants for which they pixels' distance data may be updated at each (x, y) of the iteration.

The radius of the circle varies a bit. It's equal to `gapSize` for the distance map access, and equal to `gapSize + 1` for
the opacity map access.

Next, `gapDistanceSearch` function determines which distance map pixels are updated.

```c++
gapDistanceSearch(int x, int y, int gapSize, OctantTransformation op)
{
    if (isOpaque(op(x, y, 0, -1)) || isOpaque(op(x, y, 1, -1))) {
        return;
    }

    for (int yoffs = 2; yoffs < gapSize + 2; ++yoffs) {
        int yDistanceSq = (yoffs - 1) * (yoffs - 1);

        for (int xoffs = 0; xoffs <= yoffs; ++xoffs) {
            int offsetDistance = yDistanceSq + xoffs * xoffs;

            if (offsetDistance >= 1 + gapSize * gapSize) {
                break;
            }

            if (isOpaque(op(x, y, xoffs, -yoffs))) {
                float dx = float(xoffs) / (yoffs - 1);
                float tx = 0;
                int cx = 0;

                for (int cy = 1; cy < yoffs; ++cy) {
                    updateDistanceIfSmaller(op(x, y, cx, -cy), offsetDistance);

                    tx += dx;
                    if (floor(tx) > cx) {
                        cx++;
                        updateDistanceIfSmaller(op(x, y, cx, -cy), offsetDistance);
                    }

                    updateDistanceIfSmaller(op(x, y, cx + 1, -cy), offsetDistance);
                }
            }
        }
    }
}
```

The elaborate loops above together with the coordinate transformation `op`, effectively let us walk
the pixels' surroundings and cover the shapes of the four octants. The few `isOpaque` tests help eliminate
the cases where there can be no gap. The remaining pixels converge to the smallest possible distance
at that pixel, with the use of `updateDistanceIfSmaller`.

It's worth stressing, that the distance is not updated at the pixel (x, y) itself, rather for that pixel all of its surroundings
get updated. This means, that in order to compute the final distance value at any given pixel (xd, yd),
we must run the algorithm for all the pixels in the reflected half-circle:

![area contributing to a single distance pixel](images/distance_contributing_area.png)

**Image 7:** Visualization of the pixels that contribute to the final distance value at the pixel (xd, yd).

In summary, as the whole image is being processed, multiple half-circles overlap with each other and gradually arrive
at the correct distance values at each pixel. Once this is done, our distance map is complete and we obtain a result
similar to that presented on images 4 and 5.

## Executing a gap closing fill

The basic idea of the fill algorithm is just a context-aware flood fill. Let's explain the normal flood fill first.

The fill starts at a seed point and makes use of a queue of pixels remaining to fill. The idea is very natural.
As long as there are pixels in the queue, the fill continues. Once we run out, the fill is complete.

```c++
queue.push(seedPoint);

while (queue.hasPoints()) {
    Point p = queue.getNextPoint();

    if (isNotOpaque(p.x, p.y)) {
        fill(p.x, p.y);

        // Next, try to fill the surrounding pixels:
        // to the left, right, above, and below.
        queue.push(p.x - 1, p.y);
        queue.push(p.x + 1, p.y);
        queue.push(p.x, p.y - 1);
        queue.push(p.x, p.y + 1);
    }
}
```

On top of this simple fill, in order to detect the gaps and prevent the fill from spilling to unwanted areas,
we add new information to our points. Now each point looks like this:

- position (x, y)
- distance
- whether the fill "can expand" or not

The first two are self-explanatory. The "can expand" property is related to whether we can fill from inside
a corner into an open area of lineart. The updated pseudocode loop is presented below:

```c++
seedPoint.xy = (startX, startY);                  // starting coordinates
seedPoint.distance = distanceAt(startX, startY);  // obtained from the distance map
seedPoint.canExpand = true;                       // OK to always allow it at the start

queue.push(seedPoint);

while (queue.hasPoints()) {
    Point p = queue.getNextPoint();

    if (isNotOpaque(p.x, p.y)) {
        previousDistance = p.distance
        currentDistance = distanceAt(p.x, p.y);

        allowExpand = (currentDistance >= previousDistance);
        relativeDifference = (currentDistance / previousDistance)

        if ((relativeDistance < Tolerance) || allowExpand) {
            fill(p.x, p.y);

            queue.push(p.x - 1, p.y, currentDistance, allowExpand);
            queue.push(p.x + 1, p.y, currentDistance, allowExpand);
            queue.push(p.x, p.y - 1, currentDistance, allowExpand);
            queue.push(p.x, p.y + 1, currentDistance, allowExpand);
        }
    }
}
```

We've only made subtle tweaks to the algorithm. First of all:

- When pushing the next pixel to the queue, pass along current pixel's `distance` and `allowExpand` state.
- If the pixel is not opaque and could be filled, check:
  - should we continue to expand? The expand stops if we're "descending" into the next gap (the distance is getting shorter).
  - we determine the relative distance difference compared to the previous pixel. If this number is:
    - less than 1.0 - we are getting closer to a gap
    - greater than 1.0 - we are getting away from a gap

    It's been determined empirically, that a value around `1.3` works well.

![fill process](images/fill_process.png)

**Image 8:** Visualization of a proceeding fill. The seed point is in a narrow secition of the linear, so it will
expand into the open, where the distance is "infinite". From here, it will start descending into two gaps, where
it will stop before expanding again (`allowExpand` is false at this stage).

#### The problem with the ordering

Perhaps you have noticed that it's a bit hard to guess which pixels will be filled next. Each pixel will queue its four
neighbors. As we move to the pixel after that, it may happen that the same pixel will be enqueued again! The algorithm
still works, because the `isNotOpaque` check is aware of the already filled pixels.

But there's another problem. We can take different paths to arrive at the same destination pixel. Depending on that path,
the relation of the previous and the current distance could be different, as well as the decision whether we should still allow
to expand.

It's important that this happens in a predictable way. We can achieve this predictability by using a *priority queue*
rather than a basic stack or a queue. We can require that the points in the queue are sorted in a specific way,
for example in a way that makes us first take out the pixels with the smallest distance and `allowExpand` that is false.
This detail is important to achieve a better behaved fill regardless of the starting point (well, almost).

## Optimization

Unfortunately, we're not done yet. There's one problem remaining. It's all too slow!
So what are the main issues with the described approach?

Below there are some problems listed that become really hard to ignore as the image gets big,
or if the selected gap size is large. The gap size increases the computation complexity dramatically
(there are three nested loops, bounded by the gap size).

- Computing the opacity and distance data for the whole image is very slow.
- The time to fill a small and a large area is roughly the same.
- A non-trivial amount of memory is needed for each fill (to hold the two maps).
- The flood fill algorithm is very slow compared to Krita's optimized scanline fill.

For an interactive tool such as the bucket fill, all of the above are pretty much deal-breakers.
In particular, the drag-fill mode of the contiguous fill can be prohibitively slow with this approach.

Here's how each of these problems was solved. For the actual implementation, please see the code in Krita.

### The slow flood fill algorithm

The best idea is to not use the slow algorithm. Fortunately, we can use a strict rule to select between the two available
options: the fast scanline fill, and the slow gap closing flood fill.

The fill algorithm is modified to use both methods at the same time. If gap closing is needed, then we start with the
gap closing fill. As the fill proceeds, we only fill the pixels that have a non-infinite distance. As soon as the
"infinitely" distant pixel is found, we push it onto the scanline fill's point queue. Once we run out of gap pixels,
we hand over to the scanline algorithm.

Then the scanline algorithm processes the pixels as usual, but conversely, only the pixels with distance "infinite".
As soon as a gap pixel is found (distance less than "infinite"), it's pushed onto the gap closing fill's queue. This
strategy continues in a loop, until all pixels are filled.

A very high level pseudocode is presented below:

```c++
Queue gapClosingQueue;
Queue scanlineQueue;

if (seedPoint.distance == INFINITE) {
    scanlineQueue.push(seed);
} else {
    gapClosingQueue.push(seed);
}

while (/* either queue has points */) {
    while (scanlineQueue.hasPoints())
        scanlineFill()      // will push to gapClosingQueue, if needed
    }
    while (gapClosingQueue.hasPoints()) {
        gapClosingFill();   // will push to scanlineQueue, if needed
    }
}
```

Both fill algorithms cooperate to complete the fill. In an ideal situation, the majority of pixels are in the "open area"
with "infinite" distance, which makes the fill almost as fast as the normal scanline fill.

### The cost of filling small areas

The promise of the gap closing fill is that it can stop early and not leak through a gap. If such an operation takes a long time,
it will be surprising to the user.

The best strategy to avoid the big, up-front work, is to divide the image into tiles and compute these tile-sized chunks of
distance data as needed. This way, the initial cost of starting the fill is low, and the data is computed lazily, on the fly,
as the information is needed by the fill algorithm.

Unfortunately, breaking up the image into tiles poses new challenges.

As it was mentioned before, in order to compute the distance of a pixel (xd, yd), we need to process its immediate *left* half-circle
neighborhood. If that pixel happens to lie on tile's left edge, then the required pixels are located completely outside the tile.
For the Krita implementation, a tile size of 64 by 64 pixels was chosen.

![image subdivided into tiles](images/tiles_subdivision.png)

**Image 9:** An image subdivided into tiles. Computing the distance near the tile boundary becomes a problem.
*Left:* The view of a single tile. The pixel is too close to the tile's boundary.
*Right:* Zoomed out view with the neighbor tiles. The points inside the three pink tiles must be processed, because
they contribute to pixel (xd, yd)'s distance value.

As shown above, the tile subdivision comes at a hefty price. In order to fully compute the center tile (Tx, Ty),
we are still forced to expand the calculation into the three neighboring tiles:
to the left (Tx - 1, Ty), to the top-left (Tx - 1, Ty - 1), and above (Tx, Ty - 1).

This also means that computing the distance map incrementally in tiles will carry with it an overhead of redundant computation.
If we next move to compute the tile above, we will need to process the same points again. However, this method will still
result in a better perceived latency to the user. This is especially visible if the fill ends up stopping within a small area.

In order to compute the distane values of a single tile, we need to introduce the guard bands:

![tile guard bands](images/tile_guard_bands.png)

**Image 10:** Guard bands added to the tile. The actual area that we need to process with the `gapDistanceSearch`
algorithm can be significantly larger than just the tile's bounds.

These guard bands are additional regions of the image that we must process in order to calculate the correct
distance values *inside* the tile. Their size is determined by the gap size. The example above is extreme;
With tile size of 64 pixels, the largest sensible gap we can work with is 32 pixels, because otherwise the overhead
of a single tile's calculation starts to overtake the useful work within the tile.

***Implementation note:*** Because the tile size is 64 px and the max gap size is 32 px, we need to
-- against the logic -- limit the top and bottom guard bands to 31 pixels. Remember, that the half circle represents
a pixel in which we must execute the distance search! That means, there's *another* half-circle at that point (facing right),
and it could sample the opacity map at a total distance of 32+32 pixels, which would fall on the boundary of a tile
two rows away. To simplify, we knowingly intorduce a computation error and make a decision to limit the guard band
by 1 pixel, which results in max sampling distance of 32+31=63 pixels.

### Memory allocation overhead

When testing the feature, I noticed that the drag-fill is performing exceptionally poorly, even if the filled area
is very small and the time taken by either fill algorithm (scanline and flood) is very small. The problem was with the
opacity and distance map memory allocation.

For example, for an image that is 4K resolution (3840 x 2160 pixels), both maps would take 23.7 MiB (at least).
There's no way that amount of memory could be allocated every few milliseconds without a noticable performance degradation.

The solution is simple, again. Just don't allocate the memory to throw it away immediately!

If we add a persistent cache that can be reused by the fill operations (remember, every click and pen tap is a new fill),
then the overhead can be reduced dramatically.

***Implementation note:*** Although Krita is multithreaded, there's typically only one fill operation at a time, and the
fill itself is single-threaded. This means that we can get away with allocating a single buffer and hold onto it indefinitely.
However, there are still cases when the two fill operations will happen concurrently, so the cache must be thread-safe after all.

Once the three types of optimizations mentioned above were implemented, the fill started to perform reasonably well.
There's more nuance to it, but for that it's best to just look at the source code.

## Abandoned ideas

In the course of implementation, a few other ideas were tested, but they did not perform well enough to warrant
keeping them in. I will describe them briefly.

#### Multi-threaded computation

- **Pros**
  - An exceptional speed increase.
  - Doesn't need tile subdivision.
- **Cons**
  - Must process the whole image in one go for an optimal benefit. Otherwise there's just not enough data.
  - Doesn't work with tile subdivision. Multi-threaded loading of a single tile is completely overtaken by
    the synchronization overhead.
  - The performance may be counter-intuitive. Filling a small area is just as fast/slow as filling a large area
    (for the purpose of distance map computation, at least).

Regrettably, this idea had to be scrapped because the tool's latency was too important.

#### Space-efficient opacity map representation

The current algorithm uses `uint8` for the opacity data, but the information we care about is Boolean in nature
(opaque or not). I made an attempt to pack 64 consecutive opacity pixels as `uint64`.

- **Pros**
  - Opacity data size reduction by 8.
- **Cons**
  - A more complex representation, requiring a specialized coordinate to index computation.
  - The performance was actually much worse (up to 50% slower).

Perhaps the idea was not explored sufficiently, but the initial results were discouraging.
In the interest of time, that idea was abandoned.

#### Tiling (swizzling) the data in memory

This sounds similar to the previous opacity idea, but in fact is about the placement of data in memory in order to
maximize the spatial locality, and this way achieve better CPU cache utilization. The classic approach to this
problem is using a Z-curve (aka Morton order), which employs a binary trick to efficiently compute the pixel's
coordinates in memory.

- **Pros**
  - Better spatial locality, meaning that not only pixels in the x direction are placed next to each other,
    but also the pixels in the y direction are just as close.
  - Being able to initialize a tile with a contiguous memset, because the tile appears as a linear
    slice of the array.
- **Cons**
  - More complex coordinate transformation (although with significant optimization options through compiler intrinsics).
  - Requires working with power-of-two arrays. The tiles are already 64 px in size, but this also extends
    to the image bounds. \
    This could lead to either: a) settling for a big memory overhead, b) using a less efficient
    coordinate transformation (not the classic Z-curve).

With the failed opacity experiment and the serious cons listed above, I only did some prototyping and didn't
try implementing this idea fully.

---

This concludes my brief overview of the gap closing fill for Krita! I hope the explanations herein are useful.
